<?php
/**
 * @file
 * Integration with SendPulse.
 */

define('basic_url', 'https://api.sendpulse.com');
define('grant_type', 'client_credentials');
define('client_id', variable_get('sendpulse_client_id', ''));
define('client_secret', variable_get('sendpulse_client_secret', ''));

/**
 * Implements hook_menu().
 * @return mixed
 */
function sendpulse_menu() {
  $prefix = 'admin/config/sendpulse';
  $items[$prefix] = array(
    'title' => 'SendPulse',
    'description' => t('SendPulse Tools'),
    'page callback' => 'sendpulse_admin_menu_block_page',
    'access arguments' => array('access administration pages'),
  );
  $items[$prefix . '/configure'] = array(
    'title' => 'Configure',
    'description' => t('SendPulse configuration'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sendpulse_form'),
    'access arguments' => array('access administration pages'),
  );
  return $items;
}

/**
 * Provide a single block from the administration menu as a page.
 *
 * This function is often a destination for these blocks.
 * For example, 'admin/structure/types' needs to have a destination to be valid
 * in the Drupal menu system, but too much information there might be
 * hidden, so we supply the contents of the block.
 *
 * @return
 *   The output HTML.
 */
function sendpulse_admin_menu_block_page() {
  $item = menu_get_item();
  if ($content = system_admin_menu_block($item)) {
    $output = theme('admin_block_content', array('content' => $content));
  }
  else {
    $output = t('You do not have any administrative items.');
  }
  return $output;
}

/**
 * Implements hook_form_FORM_ID_alter().
 * @param $form
 * @param $form_state
 * @param $form_id
 */
function sendpulse_form_twi_article_node_form_alter(&$form, &$form_state, $form_id) {
  module_load_include('inc', 'pathauto', NULL);
  // Check if the SendPulse configuration has values, if not, show a warning message
  if (
    variable_get('sendpulse_website_id', '') == '' ||
    variable_get('sendpulse_website_basepath', '') == '' ||
    variable_get('sendpulse_client_id', '') == '' ||
    variable_get('sendpulse_client_secret', '') == ''
  ) {
    drupal_set_message(t('You should first go to SendPulse configuration page') . ':  ' .
      l('admin/config/sendpulse/configure', 'admin/config/sendpulse/configure'), 'warning');
  }
  // Set SendPulse title
  $form['field_sendpulse_title'][LANGUAGE_NONE][0]['value']['#default_value'] =
    isset($form_state['build_info']['args'][0]->title) ? $form_state['build_info']['args'][0]->title : '';
  // Set SendPulse website id
  $form['field_sendpulse_website_id'][LANGUAGE_NONE][0]['value']['#default_value'] =
    variable_get('sendpulse_website_id', '');
  // Set SendPulse content
  $form['field_sendpulse_body'][LANGUAGE_NONE][0]['value']['#default_value'] =
    isset($form_state['build_info']['args'][0]->field_body_copy_summary[LANGUAGE_NONE][0]['value']) ?
      $form_state['build_info']['args'][0]->field_body_copy_summary[LANGUAGE_NONE][0]['value'] : '';
  // Set link
  $form['field_sendpulse_link'][LANGUAGE_NONE][0]['#default_value']['url'] =
    isset($form_state['build_info']['args'][0]->field_sendpulse_link[LANGUAGE_NONE][0]['url']) ?
      $form_state['build_info']['args'][0]->field_sendpulse_link[LANGUAGE_NONE][0]['url'] :
      variable_get('sendpulse_website_basepath', '') . 'article/' . arg(1) . '/' .
      pathauto_cleanstring($form_state['build_info']['args'][0]->title);
  // Send notification
  $form['field_sendpulse_send'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send notification'),
    '#weight' => 36,
  );
  // Make SendPulse fields required when send notification checkbox is selected
  foreach (
    array(
      'field_sendpulse_title',
      'field_sendpulse_website_id',
      'field_sendpulse_body',
      'field_sendpulse_link',
    ) as $field
  ) {
    $form[$field]['#states'] = array(
      // Only show this field when the 'toggle_me' checkbox is enabled.
      'required' => array(
        ':input[name="field_sendpulse_send"]' => array('checked' => TRUE),
      ),
    );
    $form[$field]['#element_validate'] = array('sendpulse_custom_validate');
  }

  $form['#group_children']['field_sendpulse_send'] = 'group_sendpulse';
  // Hide SendPulse fields if we are creating a new content
  if ((int)arg(1) == 0 && arg(2) != 'edit') {
    $form['field_sendpulse_markup'] = array(
      '#markup' => t('You need to save first the content, then edit it.'),
    );
    $form['#group_children']['field_sendpulse_markup'] = 'group_sendpulse';
    foreach (
      array(
        'field_sendpulse_title',
        'field_sendpulse_website_id',
        'field_sendpulse_body',
        'field_sendpulse_link',
        'field_sendpulse_send'
      ) as $field
    ) {
      $form[$field]['#access'] = FALSE;
    }
  }
}

/**
 * Custom validate function.
 * @param $element
 * @param $form_state
 */
function sendpulse_custom_validate($element, $form_state) {
  form_state_values_clean($form_state);
  if ($element[LANGUAGE_NONE]['#field_name'] == 'field_sendpulse_link') {
    $value = $element[LANGUAGE_NONE][0]['#value']['url'];
  }
  else {
    $value = $element[LANGUAGE_NONE][0]['value']['#value'];
  }
  // Check if the send pulse checkbox is checked
  if ($form_state['values']['field_sendpulse_send'] == 1) {
    if (!isset($value) || empty($value)) {
      form_error($element, $element[LANGUAGE_NONE]['#title'] . ' is required');
    }
  }
}

/**
 * Implements hook_node_submit().
 * @param $node
 * @param $form
 * @param $form_state
 */
function sendpulse_node_submit($node, $form, &$form_state) {
  form_state_values_clean($form_state);
  $params = array(
    'title'      => $form_state['values']['field_sendpulse_title'][LANGUAGE_NONE][0]['value'],
    'website_id' => $form_state['values']['field_sendpulse_website_id'][LANGUAGE_NONE][0]['value'],
    'body'       => $form_state['values']['field_sendpulse_body'][LANGUAGE_NONE][0]['value'],
    'link'       => $form_state['values']['field_sendpulse_link'][LANGUAGE_NONE][0]['url'],
  );
  // Send notification
  if ($form['field_sendpulse_send']['#value']) {
    sendpulse_send_notification($params);
  }
}

/**
 * Custom function to show the status.
 * @return null|string
 */
function sendpulse_form() {
  $form = array();
  $form['website_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Website id'),
    '#description' => t('You can get this by going to Website settings in your SendPulse. Once there,
    you can get the id from the url.'),
    '#default_value' => variable_get('sendpulse_website_id', ''),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );
  $form['website_basepath'] = array(
    '#type' => 'textfield',
    '#title' => t('Website basepath'),
    '#description' => t('This is the base url of the site. Add \'/\' at the end.'),
    '#default_value' => variable_get('sendpulse_website_basepath', ''),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );
  $form['client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Client id'),
    '#description' => 'You can get the client id in https://login.sendpulse.com/settings',
    '#default_value' => variable_get('sendpulse_client_id', ''),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );
  $form['client_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Client secret'),
    '#description' => 'You can get the client secret in https://login.sendpulse.com/settings',
    '#default_value' => variable_get('sendpulse_client_secret', ''),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['#submit'][] = "sendpulse_form_submit_handler";
  return $form;
}

/**
 * Custom submit handler.
 * @param $form
 * @param $form_state
 */
function sendpulse_form_submit_handler($form, &$form_state) {
  form_state_values_clean($form_state);
  foreach ($form_state['values'] as $key =>  $value) {
    variable_set('sendpulse_' . $key, $value);
  }
}

/**
 * Custom function to send push notifications.
 * @param $params
 * @return null|string
 */
function sendpulse_send_notification($params) {
  $SPApiProxy = new SendpulseApi(client_id, client_secret, 'session');
  // Extract variables
  extract($params);
  $task = array(
    'title'      => $title,
    'website_id' => $website_id,
    'body'       => $body,
    'ttl'        => 20,
    'stretch_time' => 10,
  );
  $additionalParams = array(
    'link' => $link,
    'filter_browsers' => 'Chrome,Safari',
    'filter_lang' => 'en',
    'filter' => ''
  );
  $SPApiProxy->createPushTask($task, $additionalParams);
}
